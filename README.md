# Pages urls

the site is deployed to the following URLs:

- [https://no-fuss-computing.gitlab.io/infrastructure/website](https://no-fuss-computing.gitlab.io/infrastructure/website)

- [http://test.nofusscomputing.com](http://test.nofusscomputing.com)
