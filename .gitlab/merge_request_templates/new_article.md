### :memo: Summary


### :link: Links / References

- LinksOrReferencesListed

#### :construction_worker: Tasks

- Required Metadata

    - [ ] meta.title

    - [ ] meta.description

    - [ ] meta.date _format YYYY-MM-DD_

    - [ ] meta.tags

        ``` yaml
        tags:
          - tagname1
          - tagname2

        ```

/target_branch development
