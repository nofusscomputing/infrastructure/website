---
template: home.html
title: No Fuss Computing
description: Here at No Fuss Computing we predominately do research and development into, making your computing experience a more enjoyable one. See our projects page to find out what we are working on.

hide:
  - navigation
  - toc
---

<div style="background: url(assets/nfc_revamp.png) no-repeat center top; background-size: 282px 90px; width: 100%; height: 120px; display: flex;">
<span style="align-self: flex-end; width: 100%; text-align: center; color: #009900; font-weight: bold; font-size: 14pt;">Using Technology to make life easier</span>
</div>

