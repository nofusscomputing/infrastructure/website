# Contact Us

We can be found in the following places on the internet:

- [:fontawesome-brands-docker:{ .docker } Docker Hub](https://hub.docker.com/u/nofusscomputing)

- [:fontawesome-brands-facebook:{ .facebook } Facebook](https://www.facebook.com/NoFussComputing)

- [:fontawesome-brands-github:{ .github } Github](https://github.com/NoFussComputing)

- [:fontawesome-brands-gitlab:{ .gitlab } Gitlab](https://gitlab.com/nofusscomputing)

- [:books: Read the Docs](https://readthedocs.org/profiles/nofusscomputing/)


## Found an issue with a page on this website?

On every page of this website that you are able to edit, you will find this icon :material-pencil:. Located in the top right hand corner of the content. After clicking on this icon, you will be taken to the git repository that contains the page in question for you to edit. An understanding of Git and Gitlab is required for the use of this method.

For the non-developers out there, you can still contribute by submitting an issue about the related page. To do this find this icon :fontawesome-brands-gitlab: on the subject page. Copy the page url to your clipboard, then click :fontawesome-brands-gitlab: icon. You will be taken to Gitlab.com and the repository in question. There will be a menu on the left hand side of the screen, where you will click issues. On the issues page loading, click new issue and use a relevant template for your report. _**Don't forget to include the url of the subject page and to ensure there is enough details for us to rectify the issue.**_

??? info "Where is the owner located??"

    Jon can be found on [:fontawesome-brands-github:{ .github } Github](https://github.com/jon-nfc) and [:fontawesome-brands-gitlab:{ .gitlab } Gitlab](https://gitlab.com/jon_nfc)
