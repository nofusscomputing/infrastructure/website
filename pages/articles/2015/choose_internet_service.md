---
title: Choosing an Internet Service
description: The Internet has now become a mainstream item within the average Australian home. The Internet has become so ingrained within our daily lives that for those of us that were around when the Internet was born have actually forgotten what life was like without it. I remember when I was growing up that if we wanted to learn about something you would go to the local library or to a family/friends house to look through their Encyclopaedia Britannica which more often than not was at least five years out of date. Believe it or not that was only 15 to 20 years ago. Now if I haven't lost you already and hopefully by the end of this article I have been able to provide you with more insight on being able to choose an Internet service from an ISP with a little more of an understanding of the technology behind that Internet connection.
date: 2015-02-25
template: article.html
type: blog
author: jon
copyrightHolder: jon
tags:
  - ADSL
  - Archive
  - Article
  - Broadband
  - Internet
  - NBN
  - Australia
---

!!! info
    This article has been targeted towards the common mass who don’t understand the technical details and considerations for choosing an Internet package.

The Internet has now become a mainstream item within the average Australian home. The Internet has become so ingrained within our daily lives that for those of us that were around when the Internet was born have actually forgotten what life was like without it. I remember when I was growing up that if we wanted to learn about something you would go to the local library or to a family/friends house to look through their Encyclopaedia Britannica which more often than not was at least five years out of date. Believe it or not that was only 15 to 20 years ago. I would consider myself lucky in the regard we were introduced to the Internet in high school way back when Web Crawler was what Google is today and the only thing of any value was searching for random phrases like <s>porn</s> homework answers. It was a very new concept to have the Internet or even access, which was often met with confusion and a sheer lack of knowledge. Being part of that generation we grew with the Internet and became the first generation to learn about it, and to the behest of the generations before us who were either left in the dark or were very slow on the uptake.

Now if I haven't lost you already and hopefully by the end of this article I have been able to provide you with more insight on being able to choose an Internet service from an ISP with a little more of an understanding of the technology behind that Internet connection.

Below I will cover a little of the technical details and hopefully bringing into perspective, so you can better understand what is being explained to you. To do this I will use the flow of water methodology.

<p hidden>#more</p>


## Bandwidth

Bandwidth is the actual speed of your Internet connection and its unit of measure is bits per second. So basically if you think water pipe, only so much water will flow through a water pipe but if you increase it’s pressure you will receive more water. For the actual Internet connection different technology is used to essentially achieve the same
thing.


## Quota

This is how much you can download and/or upload from the Internet. Its unit of measure is bytes. Think of a water tank, larger the tank the more quota you have available. This will be set by your ISP and 9 times out of 10 will be limited. Every Time you connect to the Internet you are using this available quota.


## Theory

Now to fully understand the quota and bandwidth I will have to cover a little bit of computer theory. Computers compute information by doing mathematical equations. The only numbers a computer recognizes are Base 2. Base 2 numbering means it uses the first 2 numbers, being zero and one. In common computer terms this is called binary which is always in the computer world as a minimum 8 bits wide i.e. 01010101. Each digit of the number is a bit, four bits to a nibble and 2 nibbles to a byte. Unlike the English language; Binary is read from right to left and each one or zero, within a binary number is actually counting from 1 through to 255, which takes 8 bits of information. Binary is a notational reference system, so starting from right to left each digit represents another number, the first number being one, each number after that is double the number before it so the next number would be two, then four, eight, 16, 32, 64 finishing with the leftmost digit as 128. Confusing? Not really, when the bit is zero you don't add each notated number, you only add if the bit is one. So 0001 would be one, 0010 would be two, 0011 is three and so on.

Communication between different components or computers (network or The Internet) is measured in bits per second or bps (lowercase). Computers store data in bytes or B (uppercase). Each unit of measure has multipliers like normal numbers, computers use the following multipliers, Kilo (K) for thousands, Mega (M) for millions, Giga (G) for billions, Tera (T) for trillions and Peta (P) for quintillion's. The conversion from bits to bytes is a division of 8.

So why use different units of measure for the same data? simply put, computers are electronic devices which communicate with an electronic signal with 1 being on and zero being off; basically the signal is either on or off. So the transmission of data is serial or singular and storage is in parallel or stored in octets which is a group of bits. Hence bits and bytes. generally you will find lowercase letters are used to represent bits and uppercase letters are used for bytes.


### Connection types

The common Internet connection types in Australia are ADSL or via the Mobile network. Very rarely used but still available if you look hard enough dial-up. and the newest and greatest is the National Broadband Network or NBN. With the exception of the NBN all of the above connection types will have a max speed. This max speed is a theoretical maximum speed and there are many factors which generally slow down your connection. All connection methods have a technological peak, which basically means there is no known way to improve that technology any further.


### Dial-Up

Dial-UpInternet was the first mainstream way to connect to the Internet. For this to happen you needed an active phone line, a computer with a MODEM (short for modulator/demodulator) and you would plug a phone cable into your computer, which would dial a number to connect to the Internet. At its technological peak the max theoretical download speed was 56kb and approx 33kb up. Dial-up modems are generally part of the computer. The downside to Dial-up is that it relies on good quality phone cables, basically if you have a bad phone line you will have unstable and slower Internet. Dial-up also suffers from signal attenuation which means the further away from the telephone exchange the lower the speed your Internet is going to be. The biggest downside to having Dial-up Internet is that you can either be connected to the Internet or on the phone, not both.


### ADSL

Asynchronous Digital Subscriber Line or ADSL is the most common Internet in Australia. This is also commonly referred to as broadband Internet, the biggest difference between ADSL and Dial-up is that it is always on. When ADSL first arrived it had a speed of 1.5mb, which was a massive increase over Dial-up. As ADSL evolved its speed increased, when this occurred its name slightly changed to suit the technological level, ADSL1, ADSL2 and finishing with ADSL2+ which has a maximum theoretical download speed of 24Mb and 2Mb Up. An ADSL connection like dial-up Internet requires a modem and suffers with the same line issues. Where ADSL is an improvement over Dial-up is that you can still make telephone calls when you are have an active Internet connection for this you will need a line filter. To utilise an ADSL connection you require an ADSL modem.


### Mobile Network

Again this method of connecting to the Internet has been around for a while and with the invention of the smartphone has become quite a common way to “surf the Web”. 4th generation or 4G being the current technology has a max theoretical speed of 50Mb. The connection speed is shared amongst all users utilizing that mobile tower to connect to the Internet, and if you don’t have good mobile reception you will not have a stable Internet connection.


### NBN

The National broadband network or NBN as it is known is the new way to connect to the Internet. The NBN unlike Dial-up or ADSL does not use a phone line to connect to the Internet. The NBN uses fiber optic cable, which uses light not electricity to communicate. Fiber optic cable can be thinner than a human hair. Fiber Optic cable, or fiber networks have one purpose; transmit data, not voice or electronic signals like a copper cable Since fiber is designed for networking you require a router to connect to it. A router looks very similar to an ADSL modem. Unlike the other Internet connection methods above, communication in either direction is the same. Within Australia this is not the case, and for whatever reason you will find most NBN capable ISPs will still have packages with different upload and download speeds. Fiber networks are very mature and their speed is actual, not theoretical maximums and are capable of transmitting data at 10gbps. Using that speed for connecting to the Internet is overkill and you will find that most ISPs offer no connection speed above 100mbps at the moment.


## Choosing a suitable Internet service

Now hopefully by now I have given you a little better understanding of the connection types and terminology in relation to choosing an Internet service; believe me it is still a cumbersome task. Yes there are a lot of things to consider. From a technical standpoint the following should be taken into account:

- availability in your area

- what you will be using the Internet for

- bang for buck


### Availability in Your Area

For a majority of Australians, ADSL is the best Internet service you will have available. The most annoying thing when trying to connect an ADSL service is the wait time. This wait time is due to the requirement for a technician to have to go to your local telephone exchange to actually connect you, that is if there are spare ports available in the exchange. If though, you can get the NBN, go for it. Seriously you are being foolish if you don't. Why? the NBN is an actual data network and the speed you pay for is what you get, overall the NBN is more reliable.


### What You Will be Using the Internet For?

Most website designers will aim for a page load time of within one-two seconds; Even now how website technology is becoming more media focused, I would not recommend any speed lower that ADSL2+(24mbps) or 12mbps for the NBN. Why the different speeds? Remember that ADSL is a theoretical maximum, on average. Most ADSL users will have an actual connection speed of between 8mbps-22mbps and the NBN is actual speed. Anything faster than this for just “surfing the web” is overkill, and a waste of money. Where would you require a faster connection speed? if you start to use services like netflix and/or Foxtel IPTV to stream videos. Having more than ~5 users, may also be a consideration for a speed increase. How much quota do you need? if you have had the Internet before, go look at your previous bills, most of the time you will see what you used for the billing period, use this figure as a basis to guesstimate how much quota you are going to need. If you have never had an Internet package before then as a guide I would recommend nothing less than 10GB quota per month for the average light Internet user. I can't emphasize this enough, please do your research before you take this as gospel. Most people do know someone who <s>“knows boats”</s> would be able to assist in assessing how much quota you may require.


### Bang for Buck

Read the fine print………. READ THE FINE PRINT……… oh, did I mention fine print? Every Internet package has fine print. This fine print will cover things like:

- what is unmetered((this is a category of sites that you can use without it being counted towards your quota.))

- how much quota you have, read carefully some ISPs will charge by the megabyte if you go over

- what happens when you reach your quota limit (good ISPs will shape your speed ((slow your Internet connection speed)))

- what you should not do with your Internet package (some ISPs won’t allow you to host your own website.)

- contract period

- exit fees associated with leaving your contract early

- fees associated with moving house

- support

- your responsibilities


### Further Considerations

- "**Unlimited Packages"** Some ISPs do offer "unlimited packages". Please read the fine print so you can get the definition of unlimited as sometimes unlimited does not exactly mean unlimited. Some ISPs have been know to use peak and off-peak times, during peak you will have a quota and off peak is “unlimited”. A big consideration for unlimited or large packages with lower download speeds is calculating what you could actually download in that billing period. This is a simple task and would use the following formula.

    > (Billing Period Days x seconds in day) × max download speed in bits ÷ 8 = max bytes for period
    >Example:
    >
    >- Billing Period days = 30
    >- Second in one day = 86,400
    >- Max download speed = 22,000,000
    >- ( 30 x 86,400 ) × 22,000,000 ÷ 8 = 7,128,000,000,000Bytes or 7.128TB
    >So with the example above a package with a quota above 7.128TB with a download speed of 22mb would be a waste of money.

- **Bundled Items** We have all seen and heard of them.

    - "Select our Internet Package for $120 a month and we will give you a telephone with free calls, 200gb of download and if you sign up for a 24month contract we will throw in a free iPad"

    - With the exception of a modem-if you don't have one; be very careful of falling into this trap. These “Bundled Packages” are plain and simply designed to get your business. Believe me when I say you can get all of this for a hell of a lot cheaper. Again <u>READ THE FINE PRINT</u> and calculate the cost of the whole contract period including any hidden fees and you will find that these are quite expensive packages. A simple way to test these packages is how much is thrown in, the more free stuff that is included outside of the Internet package, then the more desperate they are for your business. Listen to your gut and the age old saying "If it's too good to be true, than it probably is". Once in my life time I have seen one of these packages and it was too good to be true, but turned out to actually be worth while in the long run, again this was more than likely a “once in a lifetime good package”

- **Naked ADSL** works exactly the same as normal ADSL Internet but you don’t have to pay for the phone line as it is included in the “Naked ADSL package” if you don’t use the telephone much or you are utilizing VOIP choosing one of these packages can save you some money. Like ADSL this is only available in certain areas, Please confirm with your ISP for availability.

- **Support** It's common practice for ISPs to outsource the first stage of customer support. This problem is a plague that needs to be truncated from the IT((Information Technology)) industry. Why? 9 times out of 10 the person on the other end of the phone either does not have a good grasp on our native language or has an accent that is so thick you can not understand them, which makes it very difficult for the hearing impaired. When this occurs the assistance you called for doesn't happen, and is escalated to someone else in the chain who may or may not be able to help you. In the end you are stuck with no internet and built up frustration from a 1:30h phone call.

There are however some ISPs who are old school. they will give you a callback after being on hold for a few minutes and if your assistance call needs to be escalated will either call you back or give a detailed handover to the next person you need to speak to. bottom line if you are not technically minded than ask around to find a good ISP who can assist you and be wary of outsourced tech support.


## Conclusion

Lets face it, The Internet has made our lives easier. You can now sit in your lounge room and read this long, very mind numbing article on how to choose a better Internet Service, do your Banking, Pay Bills, Buy Goods, <s>look at porn</s> and best of all place video calls to family and friends. What will the future hold? Who knows but given how far technology has evolved in the last 50 years, I think the next 50 will yield even more from this simple little invention courtesy of DARPA.

<s>Like this article? Please consider buying me a coffee. Donations can be accepted via PayPal via our store at No Fuss Computing</s>

Some ISPs in Australia:

- Internode

- IINet

- WestNet

!!! warning
    The information contained within this article based on the opinion of the author and is provided for informational purposes only. Before utilizing any information or advice from this article you are encouraged to do your own research


!!! attention
    This post is considered an archived post.

    This article was originally posted on 25 Feb 2015. It has been migrated from its original source _http://nofusscomputing.com/wiki/public/article/150225_choose_internet_service_ to here.
